package kz.aitu.advancedJava.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "person")
public class Person {
    @Id
    private long id;
    private String firstname;
    private String lastname;
    private String city;
    private String phone;
    private String telegram;

    public void setID(long id) {
        this.id=id;
    }
}
