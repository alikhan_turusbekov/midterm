package kz.aitu.advancedJava.controller;

import org.springframework.web.bind.annotation.RequestMapping;

public class HomeController {

    @RequestMapping("/")
    public String getIndex() {
        return "index";
    }
}
