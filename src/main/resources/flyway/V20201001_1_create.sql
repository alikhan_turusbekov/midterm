create table person (
    id serial,
    firstname varchar(50),
    lastname varchar(50),
    city varchar(50),
    phone text,
    telegram varchar(50)
)